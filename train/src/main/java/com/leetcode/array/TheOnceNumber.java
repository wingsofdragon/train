package com.leetcode.array;

public class TheOnceNumber {
    public static void main(String[] args) {
    int[] nums = new int[]{1,1,3,3,5};
    int temp=0;
        for(int i=0;i<nums.length;i++){
            temp^=nums[i];
        }
        System.out.println(temp);

    }

}
